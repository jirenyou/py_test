#!/usr/bin/env python3
#coding=utf-8

#---------------------------------------------------判断语句
def my_if(num):
 if num>0:
   print ('大于0')
 else:
   print ('小于0')
   
#my_if(5)

#---------------------------------------------------遍历字典（对象）map
def foreach_map():
 _map = {"a":1,"b":2}
 for k,v in _map.items():
  #连接字符串
  str = 'key=%s  val=%s'%(k, v)
  print(str)

#foreach_map()

#---------------------------------------------------列表生成器
def list_create():
 #申明一个1-10的集合（一维数组）
 _list = list(range(1,11))
 rs = [x*x for x in _list]
 print(rs) 
 
#list_create()

#---------------------------------------------------列表生成器-带条件
def list_create_where():
 _list = list(range(1,11))
 rs = [x*x for x in _list if x%2==0]
 print(rs)
 
#list_create_where()

#---------------------------------------------------列表当前目录下所有目录和文件
def get_dir_list():
 # 导入 OS 模块
 import os
 # os.listdir 获取目录下的所有文件
 rs=[f for f in os.listdir('.')]
 print(rs)
 
#get_dir_list()

#---------------------------------------------------generator 生成器
def generator_test():
 _list=list(range(1,11))
 g=(x*x for x in _list)
 for n in g:
  print(n)
  
generator_test()