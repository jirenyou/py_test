#!/usr/bin/env python2
#coding=utf-8

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.http import Http404,HttpResponse
import datetime
import MySQLdb

# 首页视图
def index(request):
    return HttpResponse("欢迎访问首页！")

# Demo 视图
def hello(request):
    get = request.META.items()
    name = request.GET.get('jirenyuo')
    html = []
    for k, v in get:
        html.append('%s => %s' % (k, v))
        
    print(html)
    return HttpResponse("Hello world!<br/>name:%s" % name)

# 模板演示
def tpl(request):
    # 模板字典
    tpl_map = {
        'title': '模板示例',
        'name': '24K纯男'
    }
    return render_to_response('Index/index.html', tpl_map)

# 增加当前时间
# @param int offset    时间偏移量
def hoursAhead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    html = "<html><body>In %s hours(s), it will be %s . </body></html>" % (offset, dt)
    return HttpResponse(html)

# mysql 测试
def mysqltest(request):
    # 连接数据库
    conn = MySQLdb.connect(user='jirenyou', db='test', passwd='123456', host='192.168.1.10')
    cursor = conn.cursor()
    cursor.execute('select * from test')