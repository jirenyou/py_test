#coding=utf-8
"""django_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django_test.views import hello,tpl
from django_test.views import index, hello, hoursAhead
from app.views import testmodel
from Controllers import User, Admin

urlpatterns = [
    # 默认页
    url(r'^$', index),
    # 首页
    url(r'^index/', index),
    url(r'^admin/', admin.site.urls),
    url(r'^hello/', hello),
    url(r'^tpl/', tpl),
    # 时间增加
    url(r'^hoursAhead/plus/(\d{1,2})/', hoursAhead),
    # 模型
    url(r'^testmodel/', testmodel),
    # 自定义控制器
    url(r'^login/', User.login),
    # 自定义控制器
    url(r'^register/', User.register),
    url(r'^adminmanage/', Admin.index),
]
