# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

# 用户模型
class User(models.Model):
    account = models.CharField(max_length=20)
    passwd = models.CharField(max_length=32)
    name = models.CharField(max_length=20)
    sex = models.IntegerField()
    